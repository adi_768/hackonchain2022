# HackOnChain2022

We are happy to be part of the HackOnChain event at NBX 2022 in Berlin!

This is an ideal opportunity for developers and hackers to learn about Q Blockchain and its unique approach to Web3 governance. We created a list of challenges with the goal to inspire new and exciting applications for decentralized governance.

This repo will provide basic information about the Q Blockchain challenges. 

Find general HackOnChain Guide: https://web3devspl.notion.site/Hacker-Manual-41dc2c2ed87743f6a19325d095769eec

Submissions: https://web3devspl.notion.site/Submissions-e3544a99ba3d465aa9a227bdadafdf0b 

## Challenges

1. Create a DAO design that includes the Q Governance framework in its decisions. For example, you might include an expert panel to which certain decisions are delegated, or have decisions types which can be vetoed by an independent set of so-called Q root nodes. You can assume the Q governance contracts as black boxes that can interact with your contracts and provide inputs (e.g. decisions, vetos, oracle data) as an independent source.

_Example: A donation DAO that uses a DAO constitution, which states strategic goals, and a donation expert panel for the spending decisions. Integrate governance mechanisms to ensure that the DAO constitution is enforced and spending is inline with the DAO constitution._

2. Create a dispute resolution mechanism which can be plugged into DeFi protocols.

_Example: An escrow swap where participants are disputing about the swap conditions and need a neutral dispute resolver._

3. Design an NFT-based incentive for governance participation where users receive NFTs for taking part in votes. Explain what they can do with those NFTs or what other benefits the NFTs have within the Q protocol.

4. Create a prototype for a QID scheme that follows Self-Sovereign-Identity and Decentralized-Identity approaches in order to unlock a Universal Basic Income on Q. Important: One QID = one person.

5. Create a concept for a public asset fund management solution, fully integrated into the Q governance. It doesn’t need its own governance token, is managed by Q Token Holder representatives (Experts), is non-custodial and fully on-chain, while all revenue that it generates is returned to the Q Ecosystem. You can assume the Q governance contracts as black boxes that can interact with your contracts and provide inputs (e.g. decisions, vetos, oracle data) as an independent source.

_References: Fully integrated lending & borrowing on Q (live), fully integrated DEX on Q (in repository)._

## Criteria

* integration into Q governance and Q ecosystem: what parts of the ecosystem are used
* user onboarding: how to onboard crypto-natives and non-crypto-natives
* maturity of solution: loopholes considered, poor decisions avoided
* innovation: what existing solutions are re-used in new ways, what is completely new
* fun: create a meme to describe your solution and why people will love it

## References

* Parameter list of available Q governance contracts: https://hq.q.org/q-parameters
* Q JavaScript SDK to seamlessly interact with Q governance contracts: https://gitlab.com/q-dev/q-js-sdk
* Fully integrated lending & borrowing on Q:
* * borrowing: https://gitlab.com/q-dev/system-contracts/-/blob/master/contracts/defi/BorrowingCore.sol
* * saving: https://gitlab.com/q-dev/system-contracts/-/blob/master/contracts/defi/Saving.sol
* Fully integrated DEX design: https://gitlab.com/q-dev/q-dex-utils/-/tree/main/
* Q constitution: https://q.org/assets/files/Q_Constitution.pdf
* Q for the metaverse: https://medium.com/q-blockchain/q-for-the-metaverse-b6161d9da36f
* Governance, blockchain, and value creation: https://medium.com/q-blockchain/governance-blockchain-and-value-creation-fb768aaad583
* Q for DAOs: https://medium.com/q-blockchain/q-for-daos-e41b757b2af4
